/**
 * Nemanja.Tomanovic created on 17-Mar-20
 **/
public class utilsClass {


    public int add(int a, int b){
        return a + b;
    }

    public double calculateCircleArea(int radius){
        return Math.PI * radius * radius;
    }
}
