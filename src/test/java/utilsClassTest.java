import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class utilsClassTest {

    @Test
    void add() {
        utilsClass utilsClass = new utilsClass();
        int expected = 12;
        int actual = utilsClass.add(7 , 5);
        assertEquals(expected, actual);


    }

    @Test
    void calculateCircleArea() {
        utilsClass utilsClass = new utilsClass();

        assertEquals(314.1592653589793, utilsClass.calculateCircleArea(10));
    }
}